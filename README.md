# Fastcpp

This is a library for fast and high performance coding for C++

## Authors
[Gladi](https://gitlab.com/Gladi)

## License
[LGPL v3](https://www.gnu.org/licenses/lgpl-3.0.txt)
